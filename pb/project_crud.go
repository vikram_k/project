package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProjectsServiceProjectServerCrud struct {
	store ProjectStore
	bloc  ProjectsServiceProjectServerBLoC
}

type ProjectsServiceProjectServerBLoC interface {
	CreateProjectBLoC(context.Context, *CreateProjectRequest) error

	GetProjectBLoC(context.Context, *GetProjectRequest) error

	UpdateProjectBLoC(context.Context, *UpdateProjectRequest) error

	DeleteProjectBLoC(context.Context, *DeleteProjectRequest) error

	ListProjectBLoC(context.Context, *ListProjectRequest) (ProjectCondition, error)
}

func NewProjectsServiceProjectServerCrud(s ProjectStore, b ProjectsServiceProjectServerBLoC) *ProjectsServiceProjectServerCrud {
	return &ProjectsServiceProjectServerCrud{store: s, bloc: b}
}

func (s *ProjectsServiceProjectServerCrud) CreateProject(ctx context.Context, in *CreateProjectRequest) (*Project, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.Project.Id) != in.Project.GetPrefix() {
		in.Project.Id = in.Parent
	}

	ids, err := s.store.CreateProjects(ctx, in.Project)
	if err != nil {
		return nil, err
	}

	in.Project.Id = ids[0]

	return in.GetProject(), nil
}

func (s *ProjectsServiceProjectServerCrud) UpdateProject(ctx context.Context, in *UpdateProjectRequest) (*Project, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetProject().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateProject(ctx,
		in.Project, mask,
		ProjectIdEq{Id: in.Project.Id},
	); err != nil {
		return nil, err
	}

	updatedProject, err := s.store.GetProject(ctx, []string{},
		ProjectIdEq{
			Id: in.GetProject().GetId(),
		},
	)
	if err != nil {
		return nil, err
	}

	return updatedProject, nil
}

func (s *ProjectsServiceProjectServerCrud) GetProject(ctx context.Context, in *GetProjectRequest) (*Project, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	res, err := s.store.GetProject(ctx, mask, ProjectIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Project not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *ProjectsServiceProjectServerCrud) ListProject(ctx context.Context, in *ListProjectRequest) (*ListProjectResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	condition, err := s.bloc.ListProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	return s.ListWithoutPagination(ctx, condition, mask)
}

func (s *ProjectsServiceProjectServerCrud) ListWithoutPagination(ctx context.Context, condition ProjectCondition, viewMask []string) (*ListProjectResponse, error) {

	list, err := s.store.ListProjects(ctx,
		viewMask,
		condition,
	)
	if err != nil {
		return nil, err
	}

	return &ListProjectResponse{Project: list}, err
}

func (s *ProjectsServiceProjectServerCrud) DeleteProject(ctx context.Context, in *DeleteProjectRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteProjectBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteProject(ctx, ProjectIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *ProjectsServiceProjectServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
