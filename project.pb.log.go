package projects

import (
	"context"
	"encoding/json"
	"go.saastack.io/idutil"
	"google.golang.org/genproto/protobuf/field_mask"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/project/pb"
	"go.uber.org/zap"
)

type logsProjectsServer struct {
	pb.ProjectsServer
	actLogCli activityLogPb.ActivityLogsClient
	str pb.ProjectStore
}

func NewLogsProjectsServer(
	a activityLogPb.ActivityLogsClient,
	s pb.ProjectsServer,
	str pb.ProjectStore) pb.ProjectsServer {

	srv := &logsProjectsServer{s, a, str}

	return srv
}

func (s *logsProjectsServer) CreateProject(ctx context.Context, in *pb.CreateProjectRequest) (*pb.Project, error) {

	res, err := s.ProjectsServer.CreateProject(ctx, in)
	if err != nil {
		return nil, err
	}

	eventName := ".saastack.projects.v1.Projects.CreateProject"
	resActData, err := s.actLogCli.GetLinkedIdByName(ctx, &activityLogPb.GetLinkedIdByNameRequest{FullEventName: eventName})
	if err != nil {
		return nil, err
	}

	dep := &pb.Project{}
	if err = dep.Update(res, []string{string(pb.Project_Title)}); err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(dep)
	if err != nil {
		return nil, err
	}

	log := &activityLogPb.ActivityLog{
		Id:                    "",
		ActivityId:            res.Id,
		ActivityLogEventId:    resActData.EventId,
		ActivityLogTemplateId: resActData.TemplateId,
		ActivityLogData:       string(bytes),
	}
	parent := in.GetParent() // TODO : set this

	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
		Parent:      parent,
		ActivityLog: log,
	}); err != nil {
		// TODO : set Id
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", eventName), zap.String("Id", res.Id))
	}

	return res, nil
}

func (s *logsProjectsServer) GetProject(ctx context.Context, in *pb.GetProjectRequest) (*pb.Project, error) {

	res, err := s.ProjectsServer.GetProject(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *logsProjectsServer) DeleteProject(ctx context.Context, in *pb.DeleteProjectRequest) (*empty.Empty, error) {

	pro, err := s.str.GetProject(ctx, []string{string(pb.Project_Title)}, pb.ProjectAnd{
		pb.TrueCondition{},
		pb.ProjectIdEq{Id: in.Id},
	})
	if err != nil {
		return nil, err
	}

	res, err := s.ProjectsServer.DeleteProject(ctx, in)
	if err != nil {
		return nil, err
	}

	eventName := ".saastack.projects.v1.Projects.DeleteProject"
	resActData, err := s.actLogCli.GetLinkedIdByName(ctx, &activityLogPb.GetLinkedIdByNameRequest{FullEventName: eventName})
	if err != nil {
		return nil, err
	}

	delPro := &pb.Project{}
	if err = delPro.Update(pro, []string{string(pb.Project_Title)}); err != nil {
		return nil, err
	}

	bytes, err := json.Marshal(delPro)
	if err != nil {
		return nil, err
	}

	log := &activityLogPb.ActivityLog{
		Id:                    "",
		ActivityId:            in.Id,
		ActivityLogEventId:    resActData.EventId,
		ActivityLogTemplateId: resActData.TemplateId,
		ActivityLogData:       string(bytes),
	}
	parent := idutil.GetParent(in.Id)

	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
		Parent:      parent,
		ActivityLog: log,
	}); err != nil {
		// TODO : set Id
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", eventName), zap.String("Id", in.Id))
	}

	return res, nil
}

func (s *logsProjectsServer) UpdateProject(ctx context.Context, in *pb.UpdateProjectRequest) (*pb.Project, error) {

	oldPro, err := s.str.GetProject(ctx, []string{}, pb.ProjectIdEq{Id: in.GetProject().GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.ProjectsServer.UpdateProject(ctx, in)
	if err != nil {
		return nil, err
	}

	eventName := ".saastack.projects.v1.Projects.UpdateProject"
	resActData, err := s.actLogCli.GetLinkedIdByName(ctx, &activityLogPb.GetLinkedIdByNameRequest{FullEventName: eventName})
	if err != nil {
		return nil, err
	}

	masks := pb.ProjectObjectCompare(oldPro, res, "")

	objMask := []string{}
	for _, oldMask := range masks {
		if oldMask == string(pb.Project_Title) {
			continue
		}
		objMask = append(objMask, oldMask)
	}

	newLogPro := &pb.Project{}
	if err = newLogPro.Update(res, objMask); err != nil {
		return nil, err
	}
	oldLogPro := &pb.Project{}
	if err = oldLogPro.Update(oldPro, objMask); err != nil {
		return nil, err
	}

	updateProjectLog := &pb.UpdateProjectLog{
		OldProject: oldLogPro,
		NewProject: newLogPro,
		UpdateMask:    &field_mask.FieldMask{Paths: masks},
	}

	bytes, err := json.Marshal(updateProjectLog)
	if err != nil {
		return nil, err
	}

	log := &activityLogPb.ActivityLog{
		Id:                    "",
		ActivityId:            res.Id,
		ActivityLogEventId:    resActData.EventId,
		ActivityLogTemplateId: resActData.TemplateId,
		ActivityLogData:       string(bytes),
	}
	parent := idutil.GetParent(res.GetId())

	if _, err := s.actLogCli.CreateActivityLog(ctx, &activityLogPb.CreateActivityLogRequest{
		Parent:      parent,
		ActivityLog: log,
	}); err != nil {
		// TODO : set Id
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", eventName), zap.String("Id", res.Id))
	}

	return res, nil
}

func (s *logsProjectsServer) ListProject(ctx context.Context, in *pb.ListProjectRequest) (*pb.ListProjectResponse, error) {

	res, err := s.ProjectsServer.ListProject(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}
