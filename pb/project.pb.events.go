// Code generated by protoc-gen-defaults. DO NOT EDIT.

package pb

import (
	"context"
	"errors"

	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	pushglobal "go.saastack.io/deployment/events"
	events "go.saastack.io/eventspush/pb"
	"go.saastack.io/userinfo"
	"go.uber.org/zap"
)

const (
	EVENT_Projects_CreateProject pushglobal.NotificationEvent = ".saastack.projects.v1.Projects.CreateProject"

	EVENT_Projects_DeleteProject pushglobal.NotificationEvent = ".saastack.projects.v1.Projects.DeleteProject"

	EVENT_Projects_UpdateProject pushglobal.NotificationEvent = ".saastack.projects.v1.Projects.UpdateProject"
)

/*
	Receiving usage :

	switch (NotificationEvent(event.Type)){

	        case EVENT_Projects_CreateProject :

	        case EVENT_Projects_DeleteProject :
	        case EVENT_Projects_UpdateProject :

	        }
*/

type eventsProjectsServer struct {
	ProjectsServer
	eventsCli events.EventValidatorsClient
}

func NewEventsProjectsServer(
	c events.EventValidatorsClient,
	srv ProjectsServer,
) ProjectsServer {
	return &eventsProjectsServer{
		srv,
		c,
	}
}

func (s *eventsProjectsServer) CreateProject(ctx context.Context, req *CreateProjectRequest) (*Project, error) {

	res, err := s.ProjectsServer.CreateProject(ctx, req)
	if err != nil {
		return nil, err
	}

	reqData, err := ptypes.MarshalAny(req)
	if err != nil {
		ctxzap.Extract(ctx).Error("Could not create event object", zap.Any("request", req), zap.Any("response", res), zap.Error(err))
		return res, nil
	}

	resData, err := ptypes.MarshalAny(res)
	if err != nil {
		ctxzap.Extract(ctx).Error("Could not create event object", zap.Any("request", req), zap.Any("response", res), zap.Error(err))
		return res, nil
	}

	userInfo := userinfo.FromContext(ctx)
	event := events.Event{
		Type:          string(EVENT_Projects_CreateProject),
		Request:       reqData,
		Response:      resData,
		RaisedOn:      ptypes.TimestampNow(),
		RaisedBy:      userInfo.Id,
		RaisedByEmail: userInfo.Email,
	}

	if _, err = s.eventsCli.Push(ctx, &event); err != nil {
		ctxzap.Extract(ctx).Error("Could not push event", zap.Any("event", event), zap.Error(err))
	}

	return res, nil

}

func EVENT_Projects_CreateProject_Unmarshal(event *events.Event) (*CreateProjectRequest, *Project, error) {
	if event.Type != string(EVENT_Projects_CreateProject) {
		return nil, nil, errors.New("Wrong event type: Expected EVENT_Projects_CreateProject ")
	}

	var requestObj CreateProjectRequest
	if err := ptypes.UnmarshalAny(event.Request, &requestObj); err != nil {
		return nil, nil, err
	}

	var responseObj Project
	if err := ptypes.UnmarshalAny(event.Response, &responseObj); err != nil {
		return nil, nil, err
	}

	return &requestObj, &responseObj, nil
}

func (s *eventsProjectsServer) GetProject(ctx context.Context, req *GetProjectRequest) (*Project, error) {

	res, err := s.ProjectsServer.GetProject(ctx, req)
	if err != nil {
		return nil, err
	}

	return res, nil

}

func (s *eventsProjectsServer) DeleteProject(ctx context.Context, req *DeleteProjectRequest) (*empty.Empty, error) {

	res, err := s.ProjectsServer.DeleteProject(ctx, req)
	if err != nil {
		return nil, err
	}

	reqData, err := ptypes.MarshalAny(req)
	if err != nil {
		ctxzap.Extract(ctx).Error("Could not create event object", zap.Any("request", req), zap.Any("response", res), zap.Error(err))
		return res, nil
	}

	resData, err := ptypes.MarshalAny(res)
	if err != nil {
		ctxzap.Extract(ctx).Error("Could not create event object", zap.Any("request", req), zap.Any("response", res), zap.Error(err))
		return res, nil
	}

	userInfo := userinfo.FromContext(ctx)
	event := events.Event{
		Type:          string(EVENT_Projects_DeleteProject),
		Request:       reqData,
		Response:      resData,
		RaisedOn:      ptypes.TimestampNow(),
		RaisedBy:      userInfo.Id,
		RaisedByEmail: userInfo.Email,
	}

	if _, err = s.eventsCli.Push(ctx, &event); err != nil {
		ctxzap.Extract(ctx).Error("Could not push event", zap.Any("event", event), zap.Error(err))
	}

	return res, nil

}

func EVENT_Projects_DeleteProject_Unmarshal(event *events.Event) (*DeleteProjectRequest, *empty.Empty, error) {
	if event.Type != string(EVENT_Projects_DeleteProject) {
		return nil, nil, errors.New("Wrong event type: Expected EVENT_Projects_DeleteProject ")
	}

	var requestObj DeleteProjectRequest
	if err := ptypes.UnmarshalAny(event.Request, &requestObj); err != nil {
		return nil, nil, err
	}

	var responseObj empty.Empty
	if err := ptypes.UnmarshalAny(event.Response, &responseObj); err != nil {
		return nil, nil, err
	}

	return &requestObj, &responseObj, nil
}

func (s *eventsProjectsServer) UpdateProject(ctx context.Context, req *UpdateProjectRequest) (*Project, error) {

	res, err := s.ProjectsServer.UpdateProject(ctx, req)
	if err != nil {
		return nil, err
	}

	reqData, err := ptypes.MarshalAny(req)
	if err != nil {
		ctxzap.Extract(ctx).Error("Could not create event object", zap.Any("request", req), zap.Any("response", res), zap.Error(err))
		return res, nil
	}

	resData, err := ptypes.MarshalAny(res)
	if err != nil {
		ctxzap.Extract(ctx).Error("Could not create event object", zap.Any("request", req), zap.Any("response", res), zap.Error(err))
		return res, nil
	}

	userInfo := userinfo.FromContext(ctx)
	event := events.Event{
		Type:          string(EVENT_Projects_UpdateProject),
		Request:       reqData,
		Response:      resData,
		RaisedOn:      ptypes.TimestampNow(),
		RaisedBy:      userInfo.Id,
		RaisedByEmail: userInfo.Email,
	}

	if _, err = s.eventsCli.Push(ctx, &event); err != nil {
		ctxzap.Extract(ctx).Error("Could not push event", zap.Any("event", event), zap.Error(err))
	}

	return res, nil

}

func EVENT_Projects_UpdateProject_Unmarshal(event *events.Event) (*UpdateProjectRequest, *Project, error) {
	if event.Type != string(EVENT_Projects_UpdateProject) {
		return nil, nil, errors.New("Wrong event type: Expected EVENT_Projects_UpdateProject ")
	}

	var requestObj UpdateProjectRequest
	if err := ptypes.UnmarshalAny(event.Request, &requestObj); err != nil {
		return nil, nil, err
	}

	var responseObj Project
	if err := ptypes.UnmarshalAny(event.Response, &responseObj); err != nil {
		return nil, nil, err
	}

	return &requestObj, &responseObj, nil
}

func (s *eventsProjectsServer) ListProject(ctx context.Context, req *ListProjectRequest) (*ListProjectResponse, error) {

	res, err := s.ProjectsServer.ListProject(ctx, req)
	if err != nil {
		return nil, err
	}

	return res, nil

}
