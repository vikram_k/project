package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	ProjectsCreateProjectActivity = "/saastack.projects.v1.Projects/CreateProject"
	ProjectsGetProjectActivity    = "/saastack.projects.v1.Projects/GetProject"
	ProjectsDeleteProjectActivity = "/saastack.projects.v1.Projects/DeleteProject"
	ProjectsUpdateProjectActivity = "/saastack.projects.v1.Projects/UpdateProject"
	ProjectsListProjectActivity   = "/saastack.projects.v1.Projects/ListProject"
)

func RegisterProjectsActivities(cli ProjectsClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateProjectRequest) (*Project, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.CreateProject(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ProjectsCreateProjectActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetProjectRequest) (*Project, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.GetProject(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ProjectsGetProjectActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteProjectRequest) (*Empty, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.DeleteProject(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ProjectsDeleteProjectActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateProjectRequest) (*Project, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.UpdateProject(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ProjectsUpdateProjectActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListProjectRequest) (*ListProjectResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListProject(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ProjectsListProjectActivity},
	)
}

// ProjectsActivitiesClient is a typesafe wrapper for ProjectsActivities.
type ProjectsActivitiesClient struct {
}

// NewProjectsActivitiesClient creates a new ProjectsActivitiesClient.
func NewProjectsActivitiesClient(cli ProjectsClient) ProjectsActivitiesClient {
	RegisterProjectsActivities(cli)
	return ProjectsActivitiesClient{}
}

func (ca *ProjectsActivitiesClient) CreateProject(ctx workflow.Context, in *CreateProjectRequest) (*Project, error) {
	future := workflow.ExecuteActivity(ctx, ProjectsCreateProjectActivity, in)
	var result Project
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ProjectsActivitiesClient) GetProject(ctx workflow.Context, in *GetProjectRequest) (*Project, error) {
	future := workflow.ExecuteActivity(ctx, ProjectsGetProjectActivity, in)
	var result Project
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ProjectsActivitiesClient) DeleteProject(ctx workflow.Context, in *DeleteProjectRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, ProjectsDeleteProjectActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ProjectsActivitiesClient) UpdateProject(ctx workflow.Context, in *UpdateProjectRequest) (*Project, error) {
	future := workflow.ExecuteActivity(ctx, ProjectsUpdateProjectActivity, in)
	var result Project
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ProjectsActivitiesClient) ListProject(ctx workflow.Context, in *ListProjectRequest) (*ListProjectResponse, error) {
	future := workflow.ExecuteActivity(ctx, ProjectsListProjectActivity, in)
	var result ListProjectResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

const (
	ParentServiceValidateParentActivity = "/saastack.projects.v1.ParentService/ValidateParent"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateParentActivity},
	)
}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)
	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
